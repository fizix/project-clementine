﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "CyprusPlayerState.generated.h"

UCLASS()
class CYPRUSPLAYER_API ACyprusPlayerState : public APlayerState {
	GENERATED_BODY()

public:
	bool GetFallThroughPlatform() const { return bFallThroughPlatform; }
	void SetFallThroughPlatform(const bool Value) { bFallThroughPlatform = Value; }

	bool GetCanAirJump() const { return bCanAirJump; }
	void SetCanAirJump(const bool Value) { bCanAirJump = Value; }

private:
	bool bFallThroughPlatform;
	bool bCanAirJump;
};
