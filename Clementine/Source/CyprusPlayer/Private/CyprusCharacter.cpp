#include "CyprusCharacter.h"

#include "CyprusPlayerState.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "CyprusPlayer/CyprusPlayer.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"

ACyprusCharacter::ACyprusCharacter() {
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(20.f, 90.0f);

	// Don't rotate character when controller rotates
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	UCharacterMovementComponent *const Movement = GetCharacterMovement();
	// Orientation
	Movement->bOrientRotationToMovement = true; // Rotate to face direction of travel ...
	Movement->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	Movement->SetPlaneConstraintEnabled(true);
	Movement->SetPlaneConstraintAxisSetting(EPlaneConstraintAxisSetting::Y); // Lock Y coordinate to 0
	Movement->bSnapToPlaneAtStart = true;
	// Walking
	Movement->MaxWalkSpeed = 650.0f;
	Movement->BrakingFrictionFactor = 4.0f;
	// Jumping/Gravity
	Movement->GravityScale = 3.0f;
	Movement->JumpZVelocity = 1200.0f;
	Movement->AirControl = 1.0f;
	Movement->BrakingDecelerationFalling = 1024.0f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 800.0f; // The camera follows at this distance behind the character
	CameraBoom->bUsePawnControlRotation = false; // Don't rotate the camera with the character
	CameraBoom->bInheritPitch = false;
	CameraBoom->bInheritYaw = false;
	CameraBoom->bInheritRoll = false;
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	// Rotate mesh to match movement
	GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));


	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

void ACyprusCharacter::BeginPlay() {
	Super::BeginPlay();

	//Add Input Mapping Context
	const APlayerController *PlayerController = Cast<APlayerController>(Controller);
	if (PlayerController == nullptr) {
		UE_LOG(LogPlayer, Error, TEXT("Player Controller is null"));
		return;
	}

	UEnhancedInputLocalPlayerSubsystem *Subsystem =
		ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	if (Subsystem == nullptr) {
		UE_LOG(LogPlayer, Error, TEXT("Enhanced Input Subsystem is null"));
		return;
	}

	Subsystem->AddMappingContext(DefaultMappingContext, 0);
}

void ACyprusCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent *EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ACyprusCharacter::Move);

		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);
	} else {
		UE_LOG(
			LogPlayer,
			Error,
			TEXT("'%s' Failed to find an Enhanced Input component!"),
			*GetNameSafe(this)
		);
	}
}

void ACyprusCharacter::NotifyActorBeginOverlap(AActor *OtherActor) {
	Super::NotifyActorBeginOverlap(OtherActor);

	ACyprusPlayerState *State = this->GetPlayerState<ACyprusPlayerState>();

	const bool IsTouchingGround = OtherActor->ActorHasTag("Ground") || OtherActor->ActorHasTag("Platform");

	if (IsTouchingGround && State != nullptr) {
		State->SetCanAirJump(true);
	}
}

void ACyprusCharacter::Move(const FInputActionValue &Value) {
	// Early exit if there is no controller
	if (Controller == nullptr) { return; }

	// Map horizontal move input to movement along the global X axis
	const FVector2D MoveInput = Value.Get<FVector2D>();
	AddMovementInput(FVector::UnitX(), MoveInput.X);
}
