#include "CyprusPlayer/Public/CyprusPlayerController.h"

#include "CyprusPlayerState.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "GameFramework/Character.h"

void ACyprusPlayerController::BeginPlay() {
	Super::BeginPlay();
}

void ACyprusPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	UEnhancedInputComponent *EnhancedInputComponent = Cast<UEnhancedInputComponent>(this->InputComponent);

	if (EnhancedInputComponent == nullptr) {
		UE_LOG(
			LogTemp,
			Error,
			TEXT("'%s' Failed to find an Enhanced Input component!"),
			*GetNameSafe(this)
		);
		return;
	}

	// Moving
	EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ACyprusPlayerController::Move);
	EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Completed, this, &ACyprusPlayerController::Move);
}

void ACyprusPlayerController::Move(const FInputActionValue &Value) {
	const FVector2D MoveInput = Value.Get<FVector2D>();
	SetDashInput(MoveInput);

	// Map horizontal move input to movement along the global X axis
	if (ACharacter *ControlledCharacter = GetCharacter(); ControlledCharacter != nullptr) {
		ControlledCharacter->AddMovementInput(FVector::UnitX(), MoveInput.X);
	}

	// Allow the player to fall through a platform if they hold down
	ACyprusPlayerState *State = Cast<ACyprusPlayerState>(PlayerState);
	State->SetFallThroughPlatform(MoveInput.Y <= -0.5);
}

void ACyprusPlayerController::SetDashInput(const FVector2D &Value) {
	const float X = FGenericPlatformMath::Sign(Value.X);
	const float Y = FGenericPlatformMath::Sign(Value.Y);
	DashInput = FVector2D(X, Y);
}
