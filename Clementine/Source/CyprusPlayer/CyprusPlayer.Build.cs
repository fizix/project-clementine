﻿using UnrealBuildTool;

public class CyprusPlayer : ModuleRules {
	public CyprusPlayer(ReadOnlyTargetRules target) : base(target) {
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
			new[] {
				// Unreal Modules
				"Core",
				"CoreUObject",
				"Engine",
				"InputCore",
				"EnhancedInput",
			}
		);
	}
}
