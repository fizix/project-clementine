﻿#pragma once

#include "CoreMinimal.h"
#include "CyprusCharacter.h"
#include "Kismet/GameplayStatics.h"

DECLARE_LOG_CATEGORY_EXTERN(LogPlayer, Log, All);

inline bool ActorIsPlayer(AActor *Actor) {
	const ACharacter *PlayerCharacter = UGameplayStatics::GetPlayerCharacter(Actor->GetWorld(), 0);
	return PlayerCharacter != nullptr && Cast<ACharacter>(Actor) == PlayerCharacter;
}
