using UnrealBuildTool;

public class CyprusTarget : TargetRules {
	public CyprusTarget(TargetInfo target) : base(target) {
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V4;
		IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_3;
		ExtraModuleNames.Add("CyprusCore");
	}
}
