#include "Platform.h"

#include "CyprusPlayerState.h"
#include "Components/BoxComponent.h"
#include "CyprusPlayer/CyprusPlayer.h"

DEFINE_LOG_CATEGORY(LogPlatforms);

APlatform::APlatform() {
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.SetTickFunctionEnable(true);
	this->TrackPlayerInput = false;

	DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>(FName("DefaultSceneRoot"));
	SetRootComponent(DefaultSceneRoot);
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("StaticMesh"));
	StaticMesh->SetupAttachment(DefaultSceneRoot);

	Surface = CreateDefaultSubobject<UBoxComponent>(FName("Surface"));
	Surface->SetupAttachment(DefaultSceneRoot);
	Surface->SetBoxExtent(FVector(100.0f, 50.0f, 10.0f));
	Surface->SetRelativeLocation(FVector(0.0f, 0.0f, 20.0f));

	Underside = CreateDefaultSubobject<UBoxComponent>(FName("Underside"));
	Underside->SetupAttachment(DefaultSceneRoot);
	Underside->SetBoxExtent(FVector(100.0f, 50.0f, 10.0f));
	Underside->SetRelativeLocation(FVector(0.0f, 0.0f, -20.0f));
}

void APlatform::BeginPlay() {
	Super::BeginPlay();

	Underside->OnComponentBeginOverlap.AddDynamic(this, &APlatform::UndersideOverlapBegin);
	Underside->OnComponentEndOverlap.AddDynamic(this, &APlatform::UndersideOverlapEnd);
	Surface->OnComponentBeginOverlap.AddDynamic(this, &APlatform::SurfaceOverlapBegin);
	Surface->OnComponentEndOverlap.AddDynamic(this, &APlatform::SurfaceOverlapEnd);
}

void APlatform::Tick(const float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

	if (!TrackPlayerInput) { return; }

	const AController *Controller = GetWorld()->GetFirstPlayerController();

	if (Controller == nullptr) {
		UE_LOG(LogPlatforms, Error, TEXT("Failed to find player controller"));
		return;
	}

	const ACyprusPlayerState *PlayerState = Cast<ACyprusPlayerState>(Controller->PlayerState);

	if (PlayerState == nullptr) {
		UE_LOG(LogPlatforms, Error, TEXT("Failed to find player state"));
		return;
	}

	if (PlayerState->GetFallThroughPlatform()) {
		UE_LOG(LogPlatforms, Display, TEXT("Player wants to fall through platform %s"), *GetName());
		StaticMesh->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	}

}

void APlatform::UndersideOverlapBegin(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult
) {
	if (!ActorIsPlayer(OtherActor)) { return; }

	UE_LOG(LogPlatforms, Display, TEXT("Allowing player to pass through platform: %s"), *GetName());

	StaticMesh->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
}

void APlatform::UndersideOverlapEnd(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex
) {
	if (!ActorIsPlayer(OtherActor)) { return; }

	UE_LOG(LogPlatforms, Display, TEXT("Re-enabling collision for %s"), *GetName());

	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

void APlatform::SurfaceOverlapBegin(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult
) {
	if (!ActorIsPlayer(OtherActor)) { return; }

	UE_LOG(LogPlatforms, Log, TEXT("Player standing on %s"), *GetName());

	TrackPlayerInput = true;
}

void APlatform::SurfaceOverlapEnd(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex
) {
	if (!ActorIsPlayer(OtherActor)) { return; }

	UE_LOG(LogPlatforms, Log, TEXT("Player moved off %s"), *GetName());

	TrackPlayerInput = false;
}
