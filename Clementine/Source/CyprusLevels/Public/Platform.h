#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Platform.generated.h"

class UBoxComponent;

DECLARE_LOG_CATEGORY_EXTERN(LogPlatforms, All, All);

UCLASS()
class CYPRUSLEVELS_API APlatform : public AActor {
	GENERATED_BODY()

public:
	APlatform();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY()
	USceneComponent *DefaultSceneRoot;

	UPROPERTY(EditInstanceOnly)
	UStaticMeshComponent *StaticMesh;

	UPROPERTY(EditInstanceOnly)
	UBoxComponent *Surface;

	UPROPERTY(EditInstanceOnly)
	UBoxComponent *Underside;

	UFUNCTION()
	void UndersideOverlapBegin(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult &SweepResult);

	UFUNCTION()
	void UndersideOverlapEnd(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex);

	UFUNCTION()
	void SurfaceOverlapBegin(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult &SweepResult);

	UFUNCTION()
	void SurfaceOverlapEnd(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex);

private:
	bool TrackPlayerInput = false;
};
