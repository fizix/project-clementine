﻿using UnrealBuildTool;

public class CyprusLevels : ModuleRules {
	public CyprusLevels(ReadOnlyTargetRules target) : base(target) {
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
			new[] {
				// Unreal Modules
				"Core",
				"CoreUObject",
				"Engine",
				// Cyprus Modules
				"CyprusPlayer",
			}
		);
	}
}
