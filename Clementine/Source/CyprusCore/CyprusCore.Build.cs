using UnrealBuildTool;

public class CyprusCore : ModuleRules {
	public CyprusCore(ReadOnlyTargetRules target) : base(target) {
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
			new[] {
				// Unreal Modules
				"Core",
				"CoreUObject",
				"Engine",
				"InputCore",
				"EnhancedInput",
				// Cyprus Modules
				"CyprusLevels",
			}
		);
	}
}
