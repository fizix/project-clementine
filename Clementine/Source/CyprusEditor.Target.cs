using UnrealBuildTool;

public class CyprusEditorTarget : TargetRules {
	public CyprusEditorTarget(TargetInfo target) : base(target) {
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V4;
		IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_3;
		ExtraModuleNames.Add("CyprusCore");
	}
}
