<a name="readme-top"></a>

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/fizix/project-clementine">

  ![Logo](Clementine/Clementine.png)

  </a>

<h3 align="center">clementine</h3>

  <p align="center">
    <br />
    <a href="https://gitlab.com/fizix/project-clementine">
      <strong>Explore the docs »</strong>
    </a>
    <!-- TODO: 
    <br />
    <br />
    <a href="https://gitlab.com/fizix/project-clementine">View Demo</a>
    ·
    <a href="https://gitlab.com/fizix/project-clementine/issues/new?labels=bug&template=bug-report---.md">Report Bug</a>
    ·
    <a href="https://gitlab.com/fizix/project-clementine/issues/new?labels=enhancement&template=feature-request---.md">Request Feature</a> 
    -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

A 2.5D platformer that tells the story of Hermes working his way up the ranks of a delivery company to become god of messengers.

<!-- TODO: Add description here -->

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

[![Unreal][unreal-engine]][unreal-url]
[![Autodesk Maya][maya]][maya-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* [Git](https://www.git-scm.com/)
* [GitKraken](https://www.gitkraken.com/git-client/try-free)
* [GitLab account](https://gitlab.com/users/sign_up)
* Unreal Engine 5.3.2

### Setup

1. Download and install Git & GitKraken
2. Open GitKraken's settings and navigate to `Integrations/GitLab` and connect your GitLab account
3. Clone this repository by opening a new tab and pasting this URL 
   ```sh
   https://gitlab.com/fizix/project-clementine.git
   ```
4. Open this project by double clicking on `Clementine.uproject` in the `Clementine` folder

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Branches and how to use them

Branches are used to separate in-progress work from finished work.
The `main` branch is reserved for versions that can be compiled into a working game, and `develop` is used to integrate work that happens between those versions.

Active development takes place on `feature` branches, and work on levels will be done on `level` branches, for example:

```mermaid
---
config:
  gitGraph:
    parallelCommits: true
---
gitGraph LR:

  commit id: "create project"
  branch develop
  commit id: " "

  branch feature/player-controller
  commit id: "walking"
  commit id: "jumping"
  commit id: "dashing"

  checkout develop
  branch level/headquarters
  commit id: "create new map"
  commit id: "add terrain"
  commit id: "add traps"
  commit id: "add doors"

  checkout develop
  merge feature/player-controller
  checkout develop
  merge level/headquarters

  checkout main
  merge develop tag: "v0.1"
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* []()
* []()
* []()

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/fizix/project-clementine.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/fizix/project-clementine/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/fizix/project-clementine.svg?style=for-the-badge
[forks-url]: https://gitlab.com/fizix/project-clementine/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/fizix/project-clementine.svg?style=for-the-badge
[stars-url]: https://gitlab.com/fizix/project-clementine/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/open/fizix/project-clementine.svg?style=for-the-badge
[issues-url]: https://gitlab.com/fizix/project-clementine/issues
[license-shield]: https://img.shields.io/gitlab/license/fizix/project-clementine.svg?style=for-the-badge
[license-url]: https://gitlab.com/fizix/project-clementine/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: images/screenshot.png
[unreal-engine]: https://img.shields.io/badge/Unreal_Engine-000000?style=for-the-badge&logo=unrealengine
[unreal-url]: https://www.unrealengine.com
[maya]: https://img.shields.io/badge/Autodesk_Maya-000000?style=for-the-badge&logo=autodeskmaya
[maya-url]: https://www.autodesk.co.uk/products/maya
